--ADMIN
insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('','@gnmadm','gnmadm','gnom','gnmadm123','s','2019-01-01 11:45:23');

--NORMALS
insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('stef@mail.com','@stef','stef','ano','stef123','n','2019-01-01 11:45:23');

insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('coc@mail.com','@coc','coc','ainomano','coc123','n','2019-01-01 11:45:23');

insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('lam@mail.com','@lam','lam','brusco','lam123','n','2019-01-01 11:45:23');

insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('tro@mail.com','@tro','tro','tronero','tro123','n','2019-01-01 11:45:23');

insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('ming@mail.com','@ming','ming','a','ming123','n','2019-01-01 11:45:23');

insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('pim@mail.com','@pim','pim','pam','pim123','n','2019-01-01 11:45:23');

insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('tam@mail.com','@tam','tam','izar','tam123','n','2019-01-01 11:45:23');

insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('cum@mail.com','@cum','cum','shot','cum123','n','2019-01-01 11:45:23');

insert into dbo.Usuari (mail,nom_usuari,nom,cognoms,psswd,esPromotor,data_creacio)
values ('somn@mail.com','@somn','somn','i','somn123','n','2019-01-01 11:45:23');

--PROMOTORS

insert into dbo.Usr_Promotor (id_usuari,validat,cif_promotor,adreca,nom_local,num_events_creats)
values (1,'s','12345678A','','puti',0);

--TIPUS ESDV
insert into dbo.id_tipus_edv (tipus_edv) values ('musica');
insert into dbo.id_tipus_edv (tipus_edv) values ('cine');
insert into dbo.id_tipus_edv (tipus_edv) values ('exposicio');
insert into dbo.id_tipus_edv (tipus_edv) values ('fira');
insert into dbo.id_tipus_edv (tipus_edv) values ('teatre');
insert into dbo.id_tipus_edv (tipus_edv) values ('esports');
insert into dbo.id_tipus_edv (tipus_edv) values ('altres');


--ESDEVENIMENT
insert into dbo.Esdeveniment (id_promotor,tipus,nom_esdeveniment,preu,localitzacioGoogle,data_inici,data_fi,descripcio)
values (1,7,'PrimerEsdeveniment',0,'41°24''12.2"N 2°10''26.5"E','1900-01-01 11:45:23','1900-01-01 11:45:23','quedarem per tocarnos els genolls mutuament');
insert into dbo.Esdeveniment (id_promotor,tipus,nom_esdeveniment,preu,localitzacioGoogle,data_inici,data_fi,descripcio)
values (1,4,'JODER2',0,'41°24''12.2"N 2°10''26.5"E','1900-01-01 11:45:23','2019-24-12 11:45:23','nosepasquefarem');


--apuntats

insert into Apunta (id_usuari,id_esdeveniment,tipus,data_accepta,data_rebutja) values (4,1,(select tipus from esdeveniment where id_esdeveniment = 1),'2019-01-01 11:45:23',null);
insert into Apunta (id_usuari,id_esdeveniment,tipus,data_accepta,data_rebutja) values (4,2,(select tipus from esdeveniment where id_esdeveniment = 2),'2019-01-01 11:45:23','2019-01-01 11:45:23');

--Comentari

insert into dbo.Comentari (id_usuari,id_esdeveniment,tipus,comentari,data_creacio,data_modificacio,data_eliminacio)
values (1,1,(select tipus from esdeveniment where id_esdeveniment = 1),'supercomentariadlsfkjafklj','2019-01-01 11:45:23','2019-01-01 11:45:23','2019-01-01 11:45:23');

insert into dbo.Comentari (id_usuari,id_esdeveniment,tipus,comentari,data_creacio,data_modificacio,data_eliminacio)
values (5,1,(select tipus from esdeveniment where id_esdeveniment = 1),'cementeriodechampu','2019-01-01 11:45:23','2019-01-01 11:45:23','2019-01-01 11:45:23');


--DELETES

delete from  Usuari;
delete from  Usr_Promotor;
delete from  Comentari;
delete from Apunta;
delete from  Esdeveniment;
delete from  id_tipus_edv;